package triangletraverser;

import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Robin Mendzies
 */
public class TriangleTraverserGUI extends javax.swing.JFrame {
    JFileChooser fileChooser;
    File triangleInput;
    TriangleTraverser triangleTraverser;
    
    public TriangleTraverserGUI() {
        fileChooser = new JFileChooser();
        initComponents();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        radgrpCalculationMode = new javax.swing.ButtonGroup();
        lblInstructions = new javax.swing.JLabel();
        lblFileLocation = new javax.swing.JLabel();
        txtFileLocation = new javax.swing.JTextField();
        btnBrowse = new javax.swing.JButton();
        lblCalculationMode = new javax.swing.JLabel();
        rdoHighest = new javax.swing.JRadioButton();
        rdoLowest = new javax.swing.JRadioButton();
        lblResult = new javax.swing.JLabel();
        txtResult = new javax.swing.JTextField();
        btnCalculate = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Triangle Traverser");
        setLocationByPlatform(true);

        lblInstructions.setText("<html> Browse for a triangle text file. Click the buttons below to traverse the triangle from top to bottom and produce the sum of the <b>Highest Path</b> and <b>Lowest Path</b>. </html>");

        lblFileLocation.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblFileLocation.setText("Triangle File:");

        txtFileLocation.setEditable(false);

        btnBrowse.setText("Browse");
        btnBrowse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowseActionPerformed(evt);
            }
        });

        lblCalculationMode.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblCalculationMode.setText("Calculation Mode:");

        radgrpCalculationMode.add(rdoHighest);
        rdoHighest.setText("Highest Path");

        radgrpCalculationMode.add(rdoLowest);
        rdoLowest.setText("Lowest Path");

        lblResult.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblResult.setText("Result");

        txtResult.setEditable(false);

        btnCalculate.setText("Calculate!");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtResult, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblInstructions, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(rdoHighest)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(rdoLowest)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnCalculate))
                                .addComponent(lblCalculationMode)
                                .addComponent(lblResult)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtFileLocation)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnBrowse))
                                    .addComponent(lblFileLocation))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblInstructions, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblFileLocation)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtFileLocation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBrowse))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblCalculationMode)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rdoLowest)
                    .addComponent(btnCalculate)
                    .addComponent(rdoHighest))
                .addGap(18, 18, 18)
                .addComponent(lblResult)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtResult, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBrowseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowseActionPerformed
        clearForm();
        if(fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
            triangleInput = fileChooser.getSelectedFile();
            txtFileLocation.setText(triangleInput.getPath());
        }
    }//GEN-LAST:event_btnBrowseActionPerformed

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
        if(!rdoHighest.isSelected() && !rdoLowest.isSelected()){
            JOptionPane.showMessageDialog(this, "Error: Select a calculation mode");
            return;
        }
        else{
            try {
                triangleTraverser = new TriangleTraverser(triangleInput);
            }
            catch(IOException | NumberFormatException ex){
                JOptionPane.showMessageDialog(this, "Error: Problem loading and reading the file");
                return;
            }
            catch(InvalidTriangleException | NullPointerException ex){
                JOptionPane.showMessageDialog(this, "Error: Problem loading or reading the file");
                return;
            }
        }
        
        if(rdoHighest.isSelected()){
            txtResult.setText(Integer.toString(triangleTraverser.sumHighestPath()));
        }
        else if(rdoLowest.isSelected()){
            txtResult.setText(Integer.toString(triangleTraverser.sumLowestPath()));
        }        
        else{
            JOptionPane.showMessageDialog(this, "An unexpected error occured");
        }
    }//GEN-LAST:event_btnCalculateActionPerformed

    private void clearForm(){
        txtFileLocation.setText("");
        txtResult.setText("");
        radgrpCalculationMode.clearSelection();
        triangleInput = null;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TriangleTraverserGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TriangleTraverserGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TriangleTraverserGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TriangleTraverserGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TriangleTraverserGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBrowse;
    private javax.swing.JButton btnCalculate;
    private javax.swing.JLabel lblCalculationMode;
    private javax.swing.JLabel lblFileLocation;
    private javax.swing.JLabel lblInstructions;
    private javax.swing.JLabel lblResult;
    private javax.swing.ButtonGroup radgrpCalculationMode;
    private javax.swing.JRadioButton rdoHighest;
    private javax.swing.JRadioButton rdoLowest;
    private javax.swing.JTextField txtFileLocation;
    private javax.swing.JTextField txtResult;
    // End of variables declaration//GEN-END:variables
}
