package triangletraverser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Robin Mendzies
 */
public class TriangleTraverser {
    private final ArrayList<int[]> values = new ArrayList<>();

    //CONSTRUCTOR
    public TriangleTraverser(File file) throws IOException, NumberFormatException, InvalidTriangleException{
        BufferedReader br = new BufferedReader(new FileReader(file));
        String currentLine;

        while ((currentLine = br.readLine()) != null) {
            String[] stringArrayLine = currentLine.split(" ");
            int[] intArrayLine = new int[stringArrayLine.length];
            for (int i = 0; i < stringArrayLine.length; i++) {
                intArrayLine[i] = Integer.parseInt(stringArrayLine[i]);
            }
            
            values.add(intArrayLine);
        }
        
        if(values.size() <= 1){
            throw new InvalidTriangleException();
        }
    }
    
    //METHODS    
    public int sumLowestPath(){
        int row = 0, loc = 0;
        int sum = values.get(row)[loc];

        while(row++ < values.size() - 1){
            int leftChild = values.get(row)[loc];
            int rightChild = values.get(row)[loc + 1];

            if(leftChild < rightChild){
                sum += leftChild;
            }
            else{
                sum += rightChild;
                loc += 1;
            }
        }
        
        return sum;
    }

    public int sumHighestPath(){
        int row = 0, loc = 0;
        int sum = values.get(row)[loc];

        while(row++ < values.size() - 1){
            int leftChild = values.get(row)[loc];
            int rightChild = values.get(row)[loc + 1];

            if(leftChild > rightChild){
                sum += leftChild;
            }
            else{
                sum += rightChild;
                loc += 1;
            }
        }
        
        return sum;
    }
}

//EXCEPTIONS
class InvalidTriangleException extends Exception{
    String msg;

    public InvalidTriangleException() {
        msg = "Insufficient triangle rows";
    }
}